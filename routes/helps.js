let express = require('express');
let router = express.Router();

let fs = require('fs');
let path = require('path');

router.get('/1', function (req, res) {
    goMd(res, 'help1')
});

function goMd(res, name) {
    res.render('markdown', {content: getMarkdown(name), title: name})
}

function getMarkdown(_path) {
    let file = path.join(__dirname, '../markdown/' + _path + '.md');
    // return fs.readFileSync(file).toString().replaceAll('\n', '\\n');
    return new Buffer(encodeURIComponent(fs.readFileSync(file).toString())).toString('base64')
}

module.exports = router;