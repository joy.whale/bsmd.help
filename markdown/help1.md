### 帮助中心

#### 基本功能介绍
<video id="video" controls="" preload="none" poster="基本功能介绍" style="width: 100%; object-fit: cover">
      <source id="mp4" src="http://r8zwd6pks.hn-bkt.clouddn.com/%E5%9F%BA%E6%9C%AC%E5%8A%9F%E8%83%BD%E4%BB%8B%E7%BB%8D.mp4" type="video/mp4">
</video>

#### 如何使用筛选抖音
<video id="video" controls="" preload="none" poster="如何使用筛选抖音" style="width: 100%; object-fit: cover">
      <source id="mp4" src="http://r8zwd6pks.hn-bkt.clouddn.com/%E5%A6%82%E4%BD%95%E4%BD%BF%E7%94%A8%E7%AD%9B%E9%80%89%E6%8A%96%E9%9F%B3.mp4" type="video/mp4">
</video>

#### 如何使用避暑猫冬的排序功能
<video id="video" controls="" preload="none" poster="如何使用避暑猫冬的排序功能" style="width: 100%; object-fit: cover">
      <source id="mp4" src="http://r8zwd6pks.hn-bkt.clouddn.com/%E5%A6%82%E4%BD%95%E4%BD%BF%E7%94%A8%E9%81%BF%E6%9A%91%E7%8C%AB%E5%86%AC%E7%9A%84%E6%8E%92%E5%BA%8F%E5%8A%9F%E8%83%BD%282%29.mp4" type="video/mp4">
</video>